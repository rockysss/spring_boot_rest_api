package com.ankit.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RestApi36Application {

	public static void main(String[] args) {
		SpringApplication.run(RestApi36Application.class, args);
	}

}
